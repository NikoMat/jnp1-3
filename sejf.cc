#include <assert.h>
#include "sejf.h"

const int DOMYSLNY_DOSTEP = 42;

Sejf::Sejf(const std::string &tekst, int dostepy) : napis(tekst),
                                                    licznik(dostepy) {
    assert(licznik >= 0);
    wlamanie = false;
    manipulacja = false;
}

Sejf::Sejf(const std::string &tekst) : Sejf(tekst, DOMYSLNY_DOSTEP) {}

Sejf::Sejf(Sejf &&that) noexcept : napis(std::move(that.napis)),
                                   licznik(that.licznik),
                                   wlamanie(that.wlamanie),
                                   manipulacja(that.manipulacja) {}

Sejf &Sejf::operator+=(const int &n) {
    if (n >= 0) {
        licznik += n;
        manipulacja = true;
    }
    return *this;
}

Sejf &Sejf::operator-=(const int &n) {
    if (licznik - n >= 0 && n >= 0) {
        licznik -= n;
        manipulacja = true;
    }
    return *this;
}

Sejf &Sejf::operator*=(const int &n) {
    if (n > 0) {
        licznik *= n;
        manipulacja = true;
    }
    return *this;
}

Sejf &Sejf::operator=(Sejf &&that) noexcept {
    napis = std::move(that.napis);
    licznik = that.licznik;
    wlamanie = that.wlamanie;
    manipulacja = that.manipulacja;
    return *this;
}

Znak Sejf::operator[](int index) {
    if (index >= 0 && (unsigned int) index < napis.size()) {
        if (licznik > 0) {
            licznik--;
            return napis[index];
        } else { // licznik == 0
            wlamanie = true;
            return -1;
        }
    } else {
        return -1;
    }
}

const Kontroler Sejf::kontroler() {
    return Kontroler(*this);
}