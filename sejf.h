#ifndef __SEJF_H
#define __SEJF_H

#include "kontroler.h"

namespace {
    using Znak = int16_t;
}

class Kontroler;

class Sejf {

private:

    std::string napis;
    int licznik;
    bool wlamanie, manipulacja;

    /*
     * Nie pozwalamy na kopiowanie sejfów.
     */
    Sejf &operator=(const Sejf &that);

    /*
     * Nie pozwalamy na tworzenie kopii sejfów.
     */
    Sejf(const Sejf &that);

public:

    /*
     * Tworzy sejf o zawartosci podajnej jako tekst, i danej liczbie dostępów.
     */
    Sejf(const std::string &tekst, int dostepy);

    /*
     * Tworzy sejf o zawartości podanej jako tekst i domyślnej liczbie dostępów.
     */
    Sejf(const std::string &tekst);

    /*
     * kontruktor przenoszący sejf.
     */
    Sejf(Sejf &&that) noexcept;

    /*
     * zwiększa liczbę dostępów o n.
     */
    Sejf &operator+=(const int &n);

    /*
     * zmniejsza liczbę dostępów o n.
     */
    Sejf &operator-=(const int &n);

    /*
     * mnoży liczbę dostępów razy n.
     */
    Sejf &operator*=(const int &n);

    /*
     * operator przenoszący sejf.
     */
    Sejf &operator=(Sejf &&that) noexcept;

    /*
     * operator dostępu do zawartości sejfu. Każde użycie kosztuje 1 dostęp.
     */
    Znak operator[](int index);

    /*
     * funkcja zwracająca kontroler danego sejfu.
     */
    const Kontroler kontroler();

    friend class Kontroler;

    friend std::ostream &operator<<(std::ostream &strumien, const Kontroler &k);
};

#endif