#include <iostream>
#include "kontroler.h"

Kontroler::Kontroler(const Kontroler&& that) noexcept : sejf(that.sejf) {
}

Kontroler::operator bool() const {
    return (sejf.licznik > 0);
}

Kontroler::Kontroler(const Sejf &s) : sejf(s) {}

std::ostream &operator<<(std::ostream &strumien, const Kontroler &k) {
    if (k.sejf.wlamanie) {
        strumien << "ALARM: WLAMANIE\n";
    } else if (k.sejf.manipulacja) {
        strumien << "ALARM: ZMANIPULOWANY\n";
    } else {
        strumien << "OK\n";
    }
    return strumien;
}