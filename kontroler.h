#ifndef __KONTROLER_H
#define __KONTROLER_H

#include <ostream>
#include "sejf.h"

class Sejf;

class Kontroler {

private:

    const Sejf &sejf;

    /*
     * Nie pozwalamy na tworzenie kontrolera nikomu poza Sejfem.
     */
    explicit Kontroler(const Sejf &s);

    /*
     * Nie pozwalamy na kopiowanie kontrolera
     */
    Kontroler(const Kontroler& that);


public:

    /*
     * Pozwalamy na przenoszenie kontrolera
     */
    Kontroler(const Kontroler&& that) noexcept;

    /*
     * Zwraca true, jeżeli dostęp do zawartości sejfu jest możliwy.
     */
    explicit operator bool() const;

    friend std::ostream &operator<<(std::ostream &strumien, const Kontroler &k);

    friend class Sejf;
};

#endif
