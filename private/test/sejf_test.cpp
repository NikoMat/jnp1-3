#include <iostream>
#include <assert.h>
#include "../../sejf.h"

using namespace std;

int main(){
    Sejf s("a",1);
    assert (s[1] == -1);
    assert (s[-1] == -1);
    assert (s[0] == 'a');
    assert (s[0] == -1);
    s+=2;
    s+=-1;
    assert(s[0] == 'a');
    assert(s[0] == 'a');
    assert(s[0] == -1);
    s+=1;
    s+=-100;
    assert(s[0] == 'a');
    assert(s[0] == -1);
    s+=1;
    s-=-1;
    assert(s[0] == 'a');
    assert(s[0] == -1);
    s+=1;
    s-=100;
    assert(s[0] == 'a');
    assert(s[0] == -1);
    s+=1;
    s*=2;
    s*=-1;
    s*=0;
    assert(s[0] == 'a');
    assert(s[0] == 'a');
    assert(s[0] == -1);

    Sejf default_sejf("s");
    for(int i=0;i<42;i++) {
        assert(default_sejf[0] == 's');
    }
    assert(default_sejf[0]==-1);

}