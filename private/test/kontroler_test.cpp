#include <iostream>
#include <assert.h>
#include "../../sejf.h"

using namespace std;

int main(){
    Sejf s("a",1);
    Kontroler kontroler = s.kontroler();
    assert(kontroler);
    cerr << kontroler;
    s-=1;
    assert(!kontroler);
    cerr << kontroler;
    s[1];
    cerr << kontroler;
    s[0];
    cerr << kontroler;
    Kontroler kontroler2 = s.kontroler();
    cerr << kontroler2;

    s+=1000000;
    s[0];
    cerr << kontroler2;

    Sejf a("a",1),b("b",0);
    auto kontrolerA = a.kontroler();
    auto kontrolerB = b.kontroler();
    
    assert(kontrolerA);
    assert(!kontrolerB);
    swap(a,b);
    assert(!kontrolerA);
    assert(kontrolerB);
}