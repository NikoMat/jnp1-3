#include <iostream>
#include <assert.h>
#include "../../sejf.h"

using namespace std;

int main(){
    Sejf s1("a",42), s2("b",43);
    swap(s1,s2);
    assert(s1[0] == 'b');
    assert(s2[0] == 'a');

}
