#include <iostream>
#include <assert.h>
#include "../../sejf.h"

using namespace std;

int main(){
    Sejf s1("aaa", 2);
    assert(s1[0] == 'a');
    (s1 *= 3) -= 1;
    assert(s1[0] == 'a');
    assert(s1[0] == 'a');
    assert(s1[0] == -1);
    (s1 += 1) += 2;
    assert(s1[0] == 'a');
    assert(s1[0] == 'a');
    assert(s1[0] == 'a');
    assert(s1[0] == -1);
    (s1 += 5);
    (s1 -= 4) *=2;
    assert(s1[0] == 'a');
    assert(s1[0] == 'a');
    assert(s1[0] == -1);

    Sejf s6("zawartosc", 1);
    Sejf s7("zawartosc", 0);
    auto k2 = s6.kontroler();
    swap(s6, s7);
    assert(!k2);
    return 0;

}
