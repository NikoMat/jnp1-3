#include <iostream>
#include <assert.h>
#include "../../sejf.h"

using namespace std;

int main(){
    Sejf a("a",1),b("b",1),c("c",1);
    Kontroler ka = a.kontroler(), kb = b.kontroler(), kc = c.kontroler();
    if (ka + kb > kc) {
        assert(false);
    }

}
